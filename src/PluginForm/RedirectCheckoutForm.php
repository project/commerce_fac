<?php

namespace Drupal\commerce_fac\PluginForm;

use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;

class RedirectCheckoutForm extends BasePaymentOffsiteForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;

    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $configuration = $payment_gateway_plugin->getConfiguration();

    // Payment gateway configuration data.
    $data['fac_merchant_id'] = $configuration['fac_merchant_id'];
    $data['fac_acquirer_id'] = $configuration['fac_acquirer_id'];

    // Payment data.
    $data['currency'] = ($payment->getAmount()->getCurrencyCode() == 'USD') ? '840' : '388';
    $data['total'] = str_pad(str_replace(['.', ',', '$'], '', $payment->getAmount()->getNumber()), 12, '0', STR_PAD_LEFT);
    $data['variables[payment_gateway]'] = $payment->getPaymentGatewayId();
    $data['variables[order]'] = $payment->getOrderId();
    $data['order_number'] = 'FACPGTEST' . round(microtime(1) * 1000);

    // Order and billing address.
    $order = $payment->getOrder();
    // $billing_address = $order->getBillingProfile()->get('address');
    // $data['name'] = $billing_address->getGivenName() . ' ' . $billing_address->getFamilyName();
    // $data['city'] = $billing_address->getLocality();
    // $data['state'] = $billing_address->getAdministrativeArea();

    // dpm($form);

    $stringtohash = $configuration['fac_password'] . $data['fac_merchant_id'] . $data['fac_acquirer_id'] . $data['order_number'] . $data['total'] . $data['currency'];
    $hash = sha1($stringtohash, true);
    $signature = base64_encode($hash);

    $cardDetails = [
      'CardCVV2' => '123',
      'CardExpiryDate' => '0122',
      'CardNumber' => '4111111111111111',
      'IssueNumber' => '',
      'StartDate' => ''
    ];

    $transactionDetails = [
      'Amount' => str_pad(str_replace(['.', ',', '$'], '', $payment->getAmount()->getNumber()), 12, '0', STR_PAD_LEFT),
      'Currency' => ($payment->getAmount()->getCurrencyCode() == 'USD') ? '840' : '388',
      'CurrencyExponent' => 2,
      'OrderNumber' => $data['order_number'],
      'IPAddress' => \Drupal::request()->getClientIp(), //'192.168.100.64',
      'TransactionCode' => '0',
      'MerchantId' => $configuration['fac_merchant_id'],
      'Signature' => $signature,
      'AcquirerId' => $configuration['fac_acquirer_id'],
      'SignatureMethod' => 'SHA1',
    ];

    $xmlOptions = [
      "indent" => " ",
      "linebreak" => "\n",
      "typeHints" => false,
      "addDecl" => true,
      "encoding" => "UTF-8",
      "rootName" => 'Authorize3DSRequest',
      "defaultTagName" => "item",
      "rootAttributes" => [
          "xmlns" => "http://schemas.firstatlanticcommerce.com/gateway/data"
      ]
    ];

    $authorizeRequestData = [
      '@xmlns' => "http://schemas.firstatlanticcommerce.com/gateway/data",
      'TransactionDetails' => $transactionDetails,
      'CardDetails' => $cardDetails,
      'MerchantResponseURL' => $form['#return_url']
    ];

    $xmlRequest['data'] = \Drupal::service('serializer')
      ->serialize($authorizeRequestData, 'xml', [
        'xml_root_node_name' => 'Authorize3DSRequest',
        // 'xml_root_node_name' => 'AuthorizeRequest',
        'xml_format_output' => TRUE,
        'xml_encoding' => 'utf-8'
      ]);

    // $data['authorizeRequestData'] = $authorizeRequestData;

    // Form url values.
    $data['continueurl'] = $form['#return_url'];
    $data['cancelurl'] = $form['#cancel_url'];

    // $url = $payment_gateway_plugin->getApiUrl() . 'Authorize3DSRequest';
    // $url = $payment_gateway_plugin->getApiUrl() . 'AuthorizeRequest';
    $url = '/drupal8/commerce-demo/checkout/3ds-authorization-response';

    // dpm($xmlRequest);

    return $this->buildRedirectForm($form, $form_state, $url, $xmlRequest, self::REDIRECT_POST);
  }

}
