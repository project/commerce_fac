<?php
namespace Drupal\commerce_uwi_fac\Controller;

use Drupal\Core\Controller\ControllerBase;
use Exception;

/**
 * Provides route responses for the Example module.
 */
class CommerceFacController extends ControllerBase {

  /**
   * Returns a simple page.
   *
   * @return array
   *   A simple renderable array.
   */
  public function authorize3dsResponse() {
    // $values = \Drupal::request()->request->get('test');
    $values = \Drupal::request()->request->all();
    // var_dump($values['data']);
    // header("Content-Type: application/xml");
    // echo $values['data'];
    // exit;

    try {
      $url = 'https://ecm.firstatlanticcommerce.com/PGServiceXML/Authorize3DS';
      $client = \Drupal::httpClient();
      $request = $client->post($url, ['body' => $values['data']]);
      $facResponse = $request->getBody()->getContents();
      // var_dump($request);
      // var_dump($facResponse);
      // exit;
    }
    catch (\Exception $e) {
      drupal_set_message(t('An error occurred and processing did not complete.'), 'error');
      // throw new PaymentGatewayException('Payment gateway error');
      throw new Exception($e->getMessage());
    }

    $response = simplexml_load_string($facResponse);

    return [
      '#children' => $response->HTMLFormData,
    ];
  }

}
