<?php

namespace Drupal\commerce_fac\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsAuthorizationsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsVoidsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_price\Price;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Provides the First Atlantic Commerce offsite Checkout payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "first_atlantic_commerce_onsite_checkout",
 *   label = @Translation("First Atlantic Commerce (Onsite)"),
 *   display_label = @Translation("First Atlantic Commerce"),
 *    forms = {
 *     "onsite-payment" = "Drupal\commerce_fac\PluginForm\RedirectCheckoutForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "mastercard", "visa",
 *   },
 * )
 */
class FirstAtlanticCommerceOnsiteCheckout extends OnsitePaymentGatewayBase implements SupportsAuthorizationsInterface, SupportsRefundsInterface {
  public function defaultConfiguration() {
    return [
        'fac_merchant_id' => '',
        'fac_acquirer_id' => '',
        'fac_password' => ''
      ] + parent::defaultConfiguration();
  }

  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['fac_merchant_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FAC Merchant ID'),
      '#description' => $this->t('Your FAC ID provided by FAC'),
      '#default_value' => $this->configuration['fac_merchant_id'],
      '#required' => TRUE,
    ];

    $form['fac_acquirer_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FAC Acquirer ID'),
      '#description' => $this->t('The Acquirer ID for your FAC Merchant Account.'),
      '#default_value' => $this->configuration['fac_acquirer_id'],
      '#required' => TRUE,
    ];

    $form['fac_password'] = [
      '#type' => 'password',
      '#title' => $this->t('FAC Password'),
      '#description' => $this->t('The password for your FAC Merchant Account.'),
      '#default_value' => $this->configuration['fac_password'],
      '#required' => TRUE,
    ];

    return $form;
  }

  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $values = $form_state->getValue($form['#parents']);
    $this->configuration['fac_merchant_id'] = $values['fac_merchant_id'];
    $this->configuration['fac_acquirer_id'] = $values['fac_acquirer_id'];
    $this->configuration['fac_password'] = $values['fac_password'];
  }

  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    $this->assertPaymentState($payment, ['new']);
    $payment_method = $payment->getPaymentMethod();
    $this->assertPaymentMethod($payment_method);

    $amount = $payment->getAmount()->getNumber();
    $formattedAmount = str_pad(str_replace(['.', ',', '$'], '', $amount), 12, '0', STR_PAD_LEFT);
    $currency_code = $payment->getAmount()->getCurrencyCode();
    $formattedCurrencyCode = ($currency_code == 'USD') ? '840' : '388';
    $order = $payment->getOrder();
    $order_id = $payment->getOrderId();
    // $orderNumber = $order->getOrderNumber();
    // $orderNumber = 'FACPGTEST' . round(microtime(1) * 1000);

    $stringtohash = $this->configuration['fac_password'] . $this->configuration['fac_merchant_id'] . $this->configuration['fac_acquirer_id'] . $order_id . $formattedAmount . $formattedCurrencyCode;
    $hash = sha1($stringtohash, true);
    $signature = base64_encode($hash);
    // print_r($payment_method);exit;
    $cardDetails = [
      'CardCVV2' => '123',
      'CardExpiryDate' => '0322', //str_pad($payment_method->card_exp_month->value, 2, '0', STR_PAD_LEFT) . str_pad($payment_method->card_exp_year->value, 2, '0', STR_PAD_LEFT),
      'CardNumber' => '4111111111111111',//$payment_method->card_number->value,
      'IssueNumber' => '',
      'StartDate' => ''
    ];

    $transactionDetails = [
      'Amount' => str_pad(str_replace(['.', ',', '$'], '', $amount), 12, '0', STR_PAD_LEFT),
      'Currency' => ($currency_code == 'USD') ? '840' : '388',
      'CurrencyExponent' => 2,
      'OrderNumber' => $order_id,
      'IPAddress' => '192.168.100.64',
      'TransactionCode' => '0',
      'MerchantId' => $this->configuration['fac_merchant_id'],
      'Signature' => $signature,
      'AcquirerId' => $this->configuration['fac_acquirer_id'],
      'SignatureMethod' => 'SHA1',
    ];

    $xmlOptions = [
      "indent" => " ",
      "linebreak" => "\n",
      "typeHints" => false,
      "addDecl" => true,
      "encoding" => "UTF-8",
      "rootName" => 'Authorize3DSRequest',
      "defaultTagName" => "item",
      "rootAttributes" => [
          "xmlns" => "http://schemas.firstatlanticcommerce.com/gateway/data"
      ]
    ];

    $authorizeRequestData = [
      '@xmlns' => "http://schemas.firstatlanticcommerce.com/gateway/data",
      'TransactionDetails' => $transactionDetails,
      'CardDetails' => $cardDetails,
      // 'MerchantResponseURL' => $form['#return_url']
    ];

    $xmlRequestData = \Drupal::service('serializer')
      ->serialize($authorizeRequestData, 'xml', [
        // 'xml_root_node_name' => 'Authorize3DSRequest',
        'xml_root_node_name' => 'AuthorizeRequest',
        'xml_format_output' => TRUE,
        'xml_encoding' => 'utf-8'
      ]);

    try {
      $url = $this->getApiUrl() . 'Authorize';
      $client = \Drupal::httpClient();
      $request = $client->post($url, ['body' => $xmlRequestData]);
      $facResponse = $request->getBody()->getContents();
    }
    catch (\PaymentGatewayAPI\Exception $e) {
      drupal_set_message(t('An error occurred and processing did not complete.'), 'error');
      throw new PaymentGatewayException('Payment gateway error');
    }

    // $xmlOutput = \Drupal::service('serializer')->deserialize($data, \Drupal\node\Entity\Node::class, 'xml');
    $response = simplexml_load_string($facResponse);

    if ($response->CreditCardTransactionResults->ResponseCode == '1') {
      $payment->setState('authorization');
      $payment->setRemoteId($response->CreditCardTransactionResults->ReferenceNumber);
      $payment->save();

      // Capture Request
      if ($capture) {
        $transactionModificationRequestData = [
          '@xmlns' => "http://schemas.firstatlanticcommerce.com/gateway/data",
          'Amount' => str_pad(str_replace(['.', ',', '$'], '', $amount), 12, '0', STR_PAD_LEFT),
          'CurrencyExponent' => 2,
          'OrderNumber' => $order_id,
          'ModificationType' => '1',
          'MerchantId' => $this->configuration['fac_merchant_id'],
          'AcquirerId' => $this->configuration['fac_acquirer_id'],
          'Password' => $this->configuration['fac_password']
        ];

        $xmlRequestData = \Drupal::service('serializer')
          ->serialize($transactionModificationRequestData, 'xml', [
            'xml_root_node_name' => 'TransactionModificationRequest',
            'xml_format_output' => TRUE,
            'xml_encoding' => 'utf-8'
          ]);

          $url = $this->getApiUrl() . 'TransactionModification';
          $client = \Drupal::httpClient();
          $request = $client->post($url, ['body' => $xmlRequestData]);
          $facResponse = $request->getBody()->getContents();

          $transactionModificationResponse = simplexml_load_string($facResponse);

          if ($transactionModificationResponse->ResponseCode == '1') {
            $payment->setState('completed');
            $payment->setRemoteId($response->CreditCardTransactionResults->ReferenceNumber);
            $payment->save();
          }
      }
    } else  {
      drupal_set_message(t('There was an error processing this request. Reason Code:' . $response->CreditCardTransactionResults->ReasonCode), 'error');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function capturePayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['authorization']);
    // If not specified, capture the entire amount.
    $amount = $amount ?: $payment->getAmount();

    // Get Order Number
    // $order = $payment->getOrder();
    // $orderNumber = $order->getOrderNumber();

    $transactionModificationRequestData = [
      '@xmlns' => "http://schemas.firstatlanticcommerce.com/gateway/data",
      'Amount' => str_pad(str_replace(['.', ',', '$'], '', round($amount->getNumber(), 2)), 12, '0', STR_PAD_LEFT),
      'CurrencyExponent' => 2,
      'OrderNumber' => $payment->getOrderId(),
      'ModificationType' => '1',
      'MerchantId' => $this->configuration['fac_merchant_id'],
      'AcquirerId' => $this->configuration['fac_acquirer_id'],
      'Password' => $this->configuration['fac_password']
    ];

    $xmlRequestData = \Drupal::service('serializer')
      ->serialize($transactionModificationRequestData, 'xml', [
        'xml_root_node_name' => 'TransactionModificationRequest',
        'xml_format_output' => TRUE,
        'xml_encoding' => 'utf-8'
      ]);

    $url = $this->getApiUrl() . 'TransactionModification';
    $client = \Drupal::httpClient();
    $request = $client->post($url, ['body' => $xmlRequestData]);
    $facResponse = $request->getBody()->getContents();

    $transactionModificationResponse = simplexml_load_string($facResponse);

    if ($transactionModificationResponse->ResponseCode == '1') {
      $payment->setState('completed');
      $payment->setRemoteId($transactionModificationResponse->CreditCardTransactionResults->ReferenceNumber);
      $payment->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {
    $this->assertPaymentState($payment, ['authorization']);
    $amount = $payment->getAmount();

    // Get Order Number
    // $order = $payment->getOrder();
    // $orderNumber = $order->getOrderNumber();

    $transactionModificationRequestData = [
      '@xmlns' => "http://schemas.firstatlanticcommerce.com/gateway/data",
      'Amount' => str_pad(str_replace(['.', ',', '$'], '', round($amount->getNumber(), 2)), 12, '0', STR_PAD_LEFT),
      'CurrencyExponent' => 2,
      'OrderNumber' => $payment->getOrderId(),
      'ModificationType' => '3',
      'MerchantId' => $this->configuration['fac_merchant_id'],
      'AcquirerId' => $this->configuration['fac_acquirer_id'],
      'Password' => $this->configuration['fac_password']
    ];

    $xmlRequestData = \Drupal::service('serializer')
      ->serialize($transactionModificationRequestData, 'xml', [
        'xml_root_node_name' => 'TransactionModificationRequest',
        'xml_format_output' => TRUE,
        'xml_encoding' => 'utf-8'
      ]);

    $url = $this->getApiUrl() . 'TransactionModification';
    $client = \Drupal::httpClient();
    $request = $client->post($url, ['body' => $xmlRequestData]);
    $facResponse = $request->getBody()->getContents();
    // var_dump($facResponse);exit;

    $transactionModificationResponse = simplexml_load_string($facResponse);

    if ($transactionModificationResponse->ResponseCode == '1') {
      $payment->setState('authorization_voided');
      $payment->setRemoteId($payment->getRemoteId());
      $payment->save();
    } else  {
      drupal_set_message(t('There was an error processing this request. Reason Code:' . $transactionModificationResponse->ReasonCode), 'error');
    }
  }


  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['completed', 'partially_refunded']);
    // If not specified, refund the entire amount.
    $amount = $amount ?: $payment->getAmount();
    $this->assertRefundAmount($payment, $amount);

    // Get Order Number
    // $order = $payment->getOrder();
    // $orderNumber = $order->getOrderNumber();

    $transactionModificationRequestData = [
      '@xmlns' => "http://schemas.firstatlanticcommerce.com/gateway/data",
      'Amount' => str_pad(str_replace(['.', ',', '$'], '', round($amount->getNumber(), 2)), 12, '0', STR_PAD_LEFT),
      'CurrencyExponent' => 2,
      'OrderNumber' => $payment->getOrderId(),
      'ModificationType' => '2',
      'MerchantId' => $this->configuration['fac_merchant_id'],
      'AcquirerId' => $this->configuration['fac_acquirer_id'],
      'Password' => $this->configuration['fac_password']
    ];

    $xmlRequestData = \Drupal::service('serializer')
      ->serialize($transactionModificationRequestData, 'xml', [
        'xml_root_node_name' => 'TransactionModificationRequest',
        'xml_format_output' => TRUE,
        'xml_encoding' => 'utf-8'
      ]);

    $url = $this->getApiUrl() . 'TransactionModification';
    $client = \Drupal::httpClient();
    $request = $client->post($url, ['body' => $xmlRequestData]);
    $facResponse = $request->getBody()->getContents();
    // var_dump($facResponse);exit;

    $transactionModificationResponse = simplexml_load_string($facResponse);

    if ($transactionModificationResponse->ResponseCode == '1') {
      $payment->setState('refunded');
      $payment->setRemoteId($payment->getRemoteId());
      $payment->save();
    } else  {
      drupal_set_message(t('There was an error processing this request. Reason Code:' . $transactionModificationResponse->ReasonCode), 'error');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {
    $payment_method_type = $payment_method->getType()->getPluginId();
    $required_keys = [
      'payment_method_nonce',
    ];

    // @todo: Implement this
  }

  /**
   * {@inheritdoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    // @todo: Implement this

    // Delete the local entity.
    $payment_method->delete();
  }

  /**
   * {@inheritdoc}
   */
  public function getApiUrl() {
    if ($this->getMode() == 'test') {
      return 'https://ecm.firstatlanticcommerce.com/PGServiceXML/';
    }
    else {
      return 'https://marlin.firstatlanticcommerce.com/PGServiceXML/';
    }
  }
}
