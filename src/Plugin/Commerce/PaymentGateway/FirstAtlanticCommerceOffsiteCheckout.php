<?php

namespace Drupal\commerce_fac\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Symfony\Component\HttpFoundation\Request;
use Psr\Log\LoggerInterface;
use GuzzleHttp\ClientInterface;
use Drupal\commerce_order\Entity\OrderInterface;

/**
 * Provides the First Atlantic Commerce offsite Checkout payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "first_atlantic_commerce_offsite_checkout",
 *   label = @Translation("First Atlantic Commerce (Offsite)"),
 *   display_label = @Translation("FAC"),
 *    forms = {
 *     "offsite-payment" = "Drupal\commerce_fac\PluginForm\RedirectCheckoutForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "mastercard", "visa",
 *   },
 * )
 */
class FirstAtlanticCommerceOffsiteCheckout extends OffsitePaymentGatewayBase {
  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  // public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time, ClientInterface $client, LoggerInterface $logger, MessengerInterface $messenger) {
  //   parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);

  //   $this->httpClient = $client;
  //   $this->logger = $logger;
  // }


  public function defaultConfiguration() {
    return [
        'fac_merchant_id' => '',
        'fac_acquirer_id' => '',
        'fac_password' => ''
      ] + parent::defaultConfiguration();
  }

  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['fac_merchant_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FAC Merchant ID'),
      '#description' => $this->t('Your FAC ID provided by FAC'),
      '#default_value' => $this->configuration['fac_merchant_id'],
      '#required' => TRUE,
    ];

    $form['fac_acquirer_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FAC Acquirer ID'),
      '#description' => $this->t('The Acquirer ID for your FAC Merchant Account.'),
      '#default_value' => $this->configuration['fac_acquirer_id'],
      '#required' => TRUE,
    ];

    $form['fac_password'] = [
      '#type' => 'password',
      '#title' => $this->t('FAC Password'),
      '#description' => $this->t('The password for your FAC Merchant Account.'),
      '#default_value' => $this->configuration['fac_password'],
      '#required' => TRUE,
    ];

    return $form;
  }

  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $values = $form_state->getValue($form['#parents']);
    $this->configuration['fac_merchant_id'] = $values['fac_merchant_id'];
    $this->configuration['fac_acquirer_id'] = $values['fac_acquirer_id'];
    $this->configuration['fac_password'] = $values['fac_password'];
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {

    $fac_response = $request->request->all();

    // Log the response
    \Drupal::logger('commerce_fac')
      ->debug('FAC e-Commerce payment response: <pre>@body</pre>', [
        '@body' => var_export($fac_response, TRUE),
      ]);

    if ($fac_response['ResponseCode'] != '1') {
      drupal_set_message(t('Payment failed at payment gateway. Please review your information and try again'), 'error');

      throw new PaymentGatewayException($fac_response['ReasonCodeDesc'], $fac_response['ReasonCode']);
    }

    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    $payment = $payment_storage->create([
      'state' => 'authorization',
      'amount' => $order->getTotalPrice(),
      'payment_gateway' => $this->entityId,
      'order_id' => $order->id(),
      'remote_id' => $fac_response['ReferenceNo'],
    ]);

    $payment->save();

    // @todo See if we can now do the Capture
  }

  /**
   * {@inheritdoc}
   */
  public function getApiUrl() {
    if ($this->getMode() == 'test') {
      return 'https://ecm.firstatlanticcommerce.com/PGServiceXML/';
    }
    else {
      return 'https://marlin.firstatlanticcommerce.com/PGServiceXML/';
    }
  }
}
